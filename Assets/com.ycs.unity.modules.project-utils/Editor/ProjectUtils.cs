using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace YCS
{
    public class ProjectUtils : MonoBehaviour
    {
        class TreeNode
        {
            public string Name;
            public List<TreeNode> Children;

            public TreeNode(string name)
            {
                Name = name;
                Children = new List<TreeNode>();
            }

            public TreeNode Add(string name)
            {
                TreeNode newNode = new TreeNode(name);
                Children.Add(newNode);

                return newNode;
            }
        }

        [MenuItem("YCS/Init Project/Create Default Folder Structure")]
        private static void CreateFolderStructure()
        {
            TreeNode root = new TreeNode("Assets");

            TreeNode art = root.Add("Art");
            art.Add("Materials");
            art.Add("Meshes");
            art.Add("Textures");
            art.Add("Animations");
            art.Add("Fonts");

            TreeNode audio = root.Add("Audio");
            audio.Add("Music");
            audio.Add("Sounds");

            TreeNode code = root.Add("Code");
            code.Add("Scripts");
            code.Add("Shaders");

            TreeNode data = root.Add("Data");
            data.Add("ScriptableObjects");

            TreeNode level = root.Add("Level");
            level.Add("Scenes");
            level.Add("Prefabs");
            level.Add("UI");

            root.Add("ThirdParty");

            CreateFolder(root, string.Empty);

            void CreateFolder(TreeNode node, string basePath)
            {
                string folderPath = Path.Combine(basePath, node.Name);
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                if(node.Name != "Assets")
                {
                    string gitkeep = Path.Combine(folderPath, ".gitkeep");
                    if (!File.Exists(gitkeep))
                    {
                        File.Create(gitkeep).Close();
                    }
                }

                for (int i = 0; i < node.Children.Count; i++)
                {
                    CreateFolder(node.Children[i], folderPath);
                }
            }

            Debug.Log($"기본 폴더들을 생성했습니다.");
            AssetDatabase.Refresh();
        }


        [MenuItem("YCS/Init Project/Create .gitignore File")]
        private static void CreateGitIgnoreFile()
        {
            string contents =
@"
/[Ll]ibrary/
/[Tt]emp/
/[Oo]bj/
/[Bb]uild/
/[Bb]uilds/
/[Ll]ogs/
/[Uu]ser[Ss]ettings/

/[Mm]emoryCaptures/

/[Rr]ecordings/

/[Aa]ssets/Plugins/Editor/JetBrains*

.vs/
.vsconfig

.gradle/

ExportedObj/
.consulo/
*.csproj
*.unityproj
*.sln
*.suo
*.tmp
*.user
*.userprefs
*.pidb
*.booproj
*.svd
*.pdb
*.mdb
*.opendb
*.VC.db

*.pidb.meta
*.pdb.meta
*.mdb.meta

sysinfo.txt

*.apk
*.aab
*.unitypackage
*.app

crashlytics-build.properties

/[Aa]ssets/[Aa]ddressable[Aa]ssets[Dd]ata/*/*.bin*

/[Aa]ssets/[Ss]treamingAssets/aa.meta
/[Aa]ssets/[Ss]treamingAssets/aa/
";

            if (File.Exists(".gitignore"))
            {
                Debug.Log($"이미 .gitignore 파일이 존재합니다. {Directory.GetCurrentDirectory()}");
            }
            else
            {
                File.WriteAllText(".gitignore", contents);
                Debug.Log($".gitignore 파일을 생성했습니다. {Directory.GetCurrentDirectory()}");
            }
        }

        [MenuItem("YCS/Init Project/Create .gitattributes File")]
        private static void CreateGitAttributesFile()
        {
            string contents =
@"
## https://gist.github.com/nemotoo/b8a1c3a0f1225bb9231979f389fd4f3f

## Unity ##

*.cs diff=csharp text
*.cginc text
*.shader text

*.mat merge=unityyamlmerge eol=lf
*.anim merge=unityyamlmerge eol=lf
*.unity merge=unityyamlmerge eol=lf
*.prefab merge=unityyamlmerge eol=lf
*.physicsMaterial2D merge=unityyamlmerge eol=lf
*.physicMaterial merge=unityyamlmerge eol=lf
*.asset merge=unityyamlmerge eol=lf
*.meta merge=unityyamlmerge eol=lf
*.controller merge=unityyamlmerge eol=lf
LightingData.asset filter=lfs diff=lfs merge=lfs -text

## git-lfs ##

#Image
*.jpg filter=lfs diff=lfs merge=lfs -text
*.jpeg filter=lfs diff=lfs merge=lfs -text
*.png filter=lfs diff=lfs merge=lfs -text
*.gif filter=lfs diff=lfs merge=lfs -text
*.psd filter=lfs diff=lfs merge=lfs -text
*.ai filter=lfs diff=lfs merge=lfs -text

#Audio
*.mp3 filter=lfs diff=lfs merge=lfs -text
*.wav filter=lfs diff=lfs merge=lfs -text
*.ogg filter=lfs diff=lfs merge=lfs -text

#Video
*.mp4 filter=lfs diff=lfs merge=lfs -text
*.mov filter=lfs diff=lfs merge=lfs -text

#3D Object
*.FBX filter=lfs diff=lfs merge=lfs -text
*.fbx filter=lfs diff=lfs merge=lfs -text
*.blend filter=lfs diff=lfs merge=lfs -text
*.obj filter=lfs diff=lfs merge=lfs -text

#ETC
*.a filter=lfs diff=lfs merge=lfs -text
*.exr filter=lfs diff=lfs merge=lfs -text
*.tga filter=lfs diff=lfs merge=lfs -text
*.pdf filter=lfs diff=lfs merge=lfs -text
*.zip filter=lfs diff=lfs merge=lfs -text
*.dll filter=lfs diff=lfs merge=lfs -text
*.unitypackage filter=lfs diff=lfs merge=lfs -text
*.aif filter=lfs diff=lfs merge=lfs -text
*.ttf filter=lfs diff=lfs merge=lfs -text
*.rns filter=lfs diff=lfs merge=lfs -text
*.reason filter=lfs diff=lfs merge=lfs -text
*.lxo filter=lfs diff=lfs merge=lfs -text
";

            if (File.Exists(".gitattributes"))
            {
                Debug.Log($"이미 .gitattributes 파일이 존재합니다. {Directory.GetCurrentDirectory()}");
            }
            else
            {
                File.WriteAllText(".gitattributes", contents);
                Debug.Log($".gitattributes 파일을 생성했습니다. {Directory.GetCurrentDirectory()}");
            }
        }

        [MenuItem("YCS/Init Project/Create .editorconfig File")]
        private static void CreateEditorConfigFile()
        {
            string contents =
@"
[*]
charset = utf-8
";

            if (File.Exists(".editorconfig"))
            {
                Debug.Log($"이미 .editorconfig 파일이 존재합니다. {Directory.GetCurrentDirectory()}");
            }
            else
            {
                File.WriteAllText(".editorconfig", contents);
                Debug.Log($".editorconfig 파일을 생성했습니다. {Directory.GetCurrentDirectory()}");
            }
        }
    }
}